crccheck (1.3.0-1) unstable; urgency=medium

  * New upstream version "1.3.0".
  * Change upstream URLs from SourceForge to GitHub.
  * debian/control: Bump Standards-Version to "4.6.2".
  * debian/copyright:
    - Change licence of Upstream and Packaging from GPL-3+ to MIT.
    - Update copyright years.
  * debian/patches/01_nose_to_unittest.patch: Remove, upstream are using it.

 -- Marcos Talau <talau@debian.org>  Wed, 06 Sep 2023 01:58:30 +0530

crccheck (1.0-5) unstable; urgency=medium

  * Update Maintainer mail.
  * Migration of tests from nose to unittest. Consequently:
    - debian/control: Remove python3-nose from Build-Depends.
    - debian/patches/01_nose_to_unittest.patch: New. Patch to migrate.
    - Closes: #1018331.

 -- Marcos Talau <talau@debian.org>  Mon, 12 Sep 2022 13:04:15 -0300

crccheck (1.0-4) unstable; urgency=medium

  * debian/control: Bump Standards-Version to 4.6.1, no changes.
  * debian/copyright: Update packaging copyright years.

 -- Marcos Talau <marcos@talau.info>  Fri, 10 Jun 2022 16:03:06 -0300

crccheck (1.0-3) unstable; urgency=medium

  * Re-upload to unstable, using the right version from experimental, to fix
    a mistake in the last upload.

 -- Marcos Talau <marcos@talau.info>  Sun, 12 Dec 2021 14:02:29 -0300

crccheck (1.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Marcos Talau <marcos@talau.info>  Sun, 12 Dec 2021 00:07:30 -0300

crccheck (1.0-1) experimental; urgency=medium

  [ Marcos Talau ]
  * Initial release (Closes: #998614).

  [ Helmut Grohne ]
  * debian/docs
  * debian/source/*

 -- Marcos Talau <marcos@talau.info>  Tue, 07 Dec 2021 22:47:24 -0300
